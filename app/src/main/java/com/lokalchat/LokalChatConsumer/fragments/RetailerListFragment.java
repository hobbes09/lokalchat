package com.lokalchat.LokalChatConsumer.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;


import com.lokalchat.LokalChatConsumer.LokalChatApplication;
import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.activities.ChatBoardActivity;
import com.lokalchat.LokalChatConsumer.adapters.RetailerListAdapter;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.utils.SearchManager;
import com.lokalchat.LokalChatConsumer.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sudipta on 1/10/2016.
 */
public class RetailerListFragment extends Fragment {

    public static final String PRODUCT = "product";
    private List<RetailerModel> selectedRetailerList;
    private ListView lvRetailerList;
    private List retailerList;
    private ProductModel productModel;

    public static RetailerListFragment getFragment(ProductModel productModel) {
        RetailerListFragment retailerListFragment = new RetailerListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PRODUCT, productModel);
        retailerListFragment.setArguments(bundle);
        return retailerListFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        productModel = (ProductModel) bundle.get(PRODUCT);

        ((LokalChatApplication) getActivity().getApplication()).setRetailerList(new ArrayList<RetailerModel>());
        selectedRetailerList = ((LokalChatApplication) getActivity().getApplication()).getRetailerList();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_retailer_list, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);

    }

    private void initViews(View view) {

        final EditText searchEditText = (EditText) view.findViewById(R.id.edtText_search);
        searchEditText.addTextChangedListener(textWatcher);

        lvRetailerList = (ListView) view.findViewById(R.id.lv_retailer);

        retailerList = productModel.getRetailerModels();


        RetailerListAdapter listAdapter = new RetailerListAdapter(getActivity(), R.layout.list_row_retailer, retailerList);
        lvRetailerList.setAdapter(listAdapter);

        lvRetailerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.cb_retailer);
                if (!checkBox.isChecked()) {
                    checkBox.setChecked(true);
                    RetailerModel retailerModel = (RetailerModel) parent.getAdapter().getItem(position);
                    selectedRetailerList.add(retailerModel);

                } else {
                    checkBox.setChecked(false);
                    RetailerModel retailerModel = (RetailerModel) parent.getAdapter().getItem(position);
                    if (selectedRetailerList.contains(retailerModel)) {
                        selectedRetailerList.remove(retailerModel);
                    }
                }
            }
        });

        final Button btnNext = (Button) view.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selectedRetailerList.isEmpty()) {
                    startActivity(ChatBoardActivity.getIntent(getActivity(), productModel));
                } else {
                    showChooseRetailerDialog();
                }
            }
        });
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            SearchManager searchManager = new SearchManager(retailerList, s.toString(), SearchManager.SearchableEnum.RETAILER);
            searchManager.setSearchListener(searchListener);
            searchManager.initSearch();
        }
    };

    SearchManager.SearchListener searchListener = new SearchManager.SearchListener() {
        @Override
        public void onSearchSuccess(List<Object> searchedList) {
            RetailerListAdapter listAdapter = new RetailerListAdapter(getActivity(), R.layout.list_row_retailer, searchedList);
            lvRetailerList.setAdapter(listAdapter);
            lvRetailerList.invalidate();
        }

        @Override
        public void onSearchFail() {
            // TODO Localization
            //Toast.makeText(RetailerListActivity.this, "No data found!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onSearchStringEmpty() {
            RetailerListAdapter listAdapter = new RetailerListAdapter(getActivity(), R.layout.list_row_retailer, retailerList);
            lvRetailerList.setAdapter(listAdapter);
            lvRetailerList.invalidate();
        }
    };

    private void showChooseRetailerDialog() {
        Utils.showGenericDialog(getActivity(), R.string.choose_retailer, false);
    }
}
