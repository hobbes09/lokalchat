package com.lokalchat.LokalChatConsumer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.models.CityModel;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.SearchManager;
import com.lokalchat.LokalChatConsumer.utils.Utils;
import com.lokalchat.LokalChatConsumer.adapters.LocationListAdapter;

import java.util.List;

/**
 * Created by Sudipta on 8/17/2015.
 */
public class LocationActivity extends BaseActivity {

    public static final String CITY_MODEL = "city_model";
    private CityModel cityModel;
    private List locationModelList;
    private ListView locationList;

    public static Intent getIntent(Context context, CityModel cityModel) {
        Intent intent = new Intent(context, LocationActivity.class);
        intent.putExtra(CITY_MODEL, cityModel);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        cityModel = this.getIntent().getParcelableExtra(CITY_MODEL);
        setUpSupportActionBar(false, true, cityModel.getCityName());

        initViews();
    }

    private void initViews() {
        locationList = (ListView) findViewById(R.id.lv_location);

        final EditText searchEditText = (EditText) findViewById(R.id.edtText_search);
        searchEditText.addTextChangedListener(textWatcher);

        locationModelList = cityModel.getLocationModelList();
        LocationListAdapter locationListAdapter = new LocationListAdapter(this, R.layout.list_row_city, locationModelList);
        locationList.setAdapter(locationListAdapter);

        locationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String cityName = cityModel.getCityName();
                String locationName = cityModel.getLocationModelList().get(position).getLocationName();
                Utils.setPersistenceData(LocationActivity.this, Constants.CITY_PREFERENCE, cityName);
                Utils.setPersistenceData(LocationActivity.this, Constants.LOCATION_PREFERENCE, locationName);
                callLandingActivity();
            }
        });
    }

    private void callLandingActivity() {
        startActivity(LandingActivity.getIntent(this));
        finish();
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            SearchManager searchManager = new SearchManager(locationModelList, s.toString(), SearchManager.SearchableEnum.LOCATION);
            searchManager.setSearchListener(searchListener);
            searchManager.initSearch();
        }
    };

    SearchManager.SearchListener searchListener = new SearchManager.SearchListener() {
        @Override
        public void onSearchSuccess(List<Object> searchedList) {
            LocationListAdapter locationListAdapter = new LocationListAdapter(LocationActivity.this, R.layout.list_row_city, searchedList);
            locationList.setAdapter(locationListAdapter);
            locationList.invalidate();
        }

        @Override
        public void onSearchFail() {
            // TODO Localization
            //Toast.makeText(CityActivity.this, "No data found!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onSearchStringEmpty() {
            LocationListAdapter locationListAdapter = new LocationListAdapter(LocationActivity.this, R.layout.list_row_city, locationModelList);
            locationList.setAdapter(locationListAdapter);
            locationList.invalidate();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cancel, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_close) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
