package com.lokalchat.LokalChatConsumer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.crashlog.CustomExceptionHandler;
import com.lokalchat.LokalChatConsumer.listener.AuthenticationListener;
import com.lokalchat.LokalChatConsumer.listener.CategoryListener;
import com.lokalchat.LokalChatConsumer.models.AuthenticationModel;
import com.lokalchat.LokalChatConsumer.models.CategoryModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.task.AuthenticationTask;
import com.lokalchat.LokalChatConsumer.task.CategoryTask;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;

import java.util.List;
import java.util.Timer;


public class SplashActivity extends Activity {

    private final int SPLASH_TIMEOUT_DELAY = 3000;
    private String authenticatedPhoneNumber;
    private boolean isRegistered;
    private boolean hasActivityDestroyed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler());
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasActivityDestroyed = false;

        callTimeTask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    private void callAuthenticationFailure() {
        // TODO Handle Authentication Failure.
    }

    private void callTimeTask() {
        final Timer timer = new Timer();

        if (TextUtils.isEmpty(Utils.getPersistenceData(this, Constants.REGISTERED_PHONE_PREFERENCE))) {
            callSignUpActivity();
        } else {
            initAuthentication();
        }


    }

    private void callOTPActivity() {

        Intent intent = new Intent(this, OTPActivity.class);
        startActivity(intent);

    }

    private void initCategoryFetch() {

        final CategoryListener categoryListener = new CategoryListener() {
            @Override
            public void onCategoryFetchStart() {

            }

            @Override
            public void onCategoryFetchSuccess(List<CategoryModel> categoryModelList) {
                if (!categoryModelList.isEmpty()) {
                    Utils.getShopChatApplication(SplashActivity.this).setCategoryModelList(categoryModelList);
                    callLandingActivity();
                } else {
                    Utils.showGenericDialog(SplashActivity.this, R.string.no_data, isActivityDestroyed());
                }

            }

            @Override
            public void onCategoryFetchFailure(ErrorModel errorModel) {

                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        break;
                }

            }
        };

        CategoryTask categoryTask = new CategoryTask(this, categoryListener);
        categoryTask.fetchCategory();
    }

    private void callChangePasswordActivity() {
        startActivity(ChangePasswordActivity.getIntent(this));
        finish();
    }

    private void initAuthentication() {

        final AuthenticationListener authenticationListener = new AuthenticationListener() {
            @Override
            public void onAuthenticationSuccess() {

                initCategoryFetch();

            }

            @Override
            public void onAuthenticationFailure(ErrorModel errorModel) {
                // TODO Localization
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_UNAUTHORIZED:
                        if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.OTP_VERIFICATION_REQ)) {
                            callOTPActivity();
                        } else if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.USER_NOT_FOUND)) {
                            callSignUpActivity();
                        } else if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.CHANGE_PASSWORD)) {
                            callChangePasswordActivity();
                        } else {
                            Utils.showGenericDialog(SplashActivity.this
                                    , errorModel.getErrorMessage(), isActivityDestroyed());
                        }

                        break;
                    default:
                        break;
                }

            }
        };

        AuthenticationModel authenticationModel = new AuthenticationModel();
        String userId = Utils.getPersistenceData(this, Constants.REGISTERED_PHONE_PREFERENCE);
        authenticationModel.setUserId(userId);
        authenticationModel.setPassword(Utils.getPersistenceData(this, userId));

        AuthenticationTask authenticationTask = new AuthenticationTask(this,
                authenticationListener, authenticationModel);
        authenticationTask.authenticate();
    }


    private void callLandingActivity() {
        startActivity(LandingActivity.getIntent(this));
        finish();
    }

    private void callSignUpActivity() {
        startActivity(SignUpActivity.getIntent(this));
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
