package com.lokalchat.LokalChatConsumer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.ListView;


import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.adapters.RetailerPageFragmentAdapter;
import com.lokalchat.LokalChatConsumer.fragments.RetailerListFragment;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.views.SlidingTabLayout;

import java.util.List;

/**
 * Created by Sudipta on 1/10/2016.
 */
public class RetailerBaseActivity extends BaseActivity {
    public static final String PRODUCT = "product";
    private List<RetailerModel> selectedRetailerList;
    private ListView lvRetailerList;
    private List retailerList;
    private ProductModel productModel;
    private RetailerPageFragmentAdapter adapter;
    private ViewPager viewPager;
    private SlidingTabLayout slidingTabLayout;

    public static Intent getIntent(Context context, ProductModel productModel) {
        Intent intent = new Intent(context, RetailerBaseActivity.class);
        intent.putExtra(PRODUCT, productModel);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retailer_base);
        productModel = this.getIntent().getParcelableExtra(PRODUCT);
        setUpSupportActionBar(false, false, "Retailer");

        initViews();

    }

    private void initViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            adapter = new RetailerPageFragmentAdapter(getFragmentManager());
        } else {
            adapter = new RetailerPageFragmentAdapter(getFragmentManager());
            adapter.setReuseFragments(false);
        }

        RetailerListFragment retailerListFragment = (RetailerListFragment) RetailerListFragment.getFragment(productModel);


        adapter.addFragment(getString(R.string.chat_fragment), retailerListFragment);
      /*  adapter.addFragment(getString(R.string.home_fragment), retailerListFragment);
        adapter.addFragment(getString(R.string.profile_fragment), retailerListFragment);*/

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);

        viewPager.setAdapter(adapter);
        slidingTabLayout.setViewPager(viewPager);
    }
}
