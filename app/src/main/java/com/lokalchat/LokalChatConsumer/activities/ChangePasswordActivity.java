package com.lokalchat.LokalChatConsumer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.listener.PasswordChangeListener;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.task.PasswordChangeTask;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;
import com.lokalchat.LokalChatConsumer.views.CustomProgress;

/**
 * Created by Sudipta on 1/29/2016.
 */
public class ChangePasswordActivity extends BaseActivity {

    private LinearLayout llOldPassword;
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmPassword;
    private Button btnDone;
    private static Context callingActivity;
    private boolean hasActivityDestroyed;
    private CustomProgress progressDialog;
    private TextView tvPassword;

    public static Intent getIntent(Context context) {
        callingActivity = context;
        Intent intent = new Intent(context, ChangePasswordActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        if (callingActivity instanceof SplashActivity) {
            setUpSupportActionBar(false, false, getString(R.string.change_password));
            changeHamburgerIcon(R.drawable.ic_launcher);

        } else {
            setUpSupportActionBar(false, true, getString(R.string.change_password));
        }


        initViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    private void initViews() {

        llOldPassword = (LinearLayout) findViewById(R.id.ll_old_password);
        edtOldPassword = (EditText) findViewById(R.id.edt_old_password);
        edtNewPassword = (EditText) findViewById(R.id.edt_new_password);
        edtConfirmPassword = (EditText) findViewById(R.id.edt_confirm_password);
        btnDone = (Button) findViewById(R.id.btn_done);
        tvPassword = (TextView)findViewById(R.id.tv_password);

        if (callingActivity instanceof SplashActivity) {
            llOldPassword.setVisibility(View.GONE);
            tvPassword.setText(getString(R.string.set_password));
        }else{
            tvPassword.setText(getString(R.string.new_password));
        }

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasFieldValidated()) {
                    initChangePasswordTask();
                }
            }
        });

    }

    private void initChangePasswordTask() {
        progressDialog = Utils.getProgressDialog(this);

        PasswordChangeListener passwordChangeListener = new PasswordChangeListener() {

            @Override
            public void onPasswordChangeStart() {
                progressDialog.show();
            }

            @Override
            public void onPasswordChangeSuccess() {
                progressDialog.dismiss();
                Toast.makeText(ChangePasswordActivity.this, R.string.password_change_sucess, Toast.LENGTH_LONG).show();
                savePasswordInPreference();
                callSplashActivity();
            }

            @Override
            public void onPasswordChangeFailure(ErrorModel errorModel) {
                progressDialog.dismiss();
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(ChangePasswordActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(ChangePasswordActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        break;
                }
            }


        };

        String userName = Utils.getPersistenceData(this, Constants.REGISTERED_PHONE_PREFERENCE);
        String oldPassword = edtOldPassword.getText().toString().trim();
        String newPassword = edtConfirmPassword.getText().toString().trim();

        PasswordChangeTask passwordChangeTask = new PasswordChangeTask(this, passwordChangeListener, userName, oldPassword,
                newPassword);
        passwordChangeTask.initPasswordChange();
    }

    private void callSplashActivity() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void savePasswordInPreference() {

        Utils.setPersistenceData(this, Utils.getPersistenceData(this, Constants.REGISTERED_PHONE_PREFERENCE), edtConfirmPassword.getText().toString());
    }

    private boolean hasFieldValidated() {
        boolean hasValidated = false;
        if (llOldPassword.isShown() && edtOldPassword.length() == 0) {
            Utils.showGenericDialog(this, R.string.enter_old_password, isActivityDestroyed());
        } else if (edtNewPassword.length() == 0) {
            Utils.showGenericDialog(this, R.string.enter_new_password, isActivityDestroyed());
        } else if (edtConfirmPassword.length() == 0) {
            Utils.showGenericDialog(this, R.string.enter_confirm_password, isActivityDestroyed());
        } else if (!edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            Utils.showGenericDialog(this, R.string.password_not_match, isActivityDestroyed());
        } else {
            hasValidated = true;
        }
        return hasValidated;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }
}
