package com.lokalchat.LokalChatConsumer.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.lokalchat.LokalChatConsumer.LokalChatApplication;
import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.adapters.ChatBoardListAdapter;
import com.lokalchat.LokalChatConsumer.adapters.FaqAdapter;
import com.lokalchat.LokalChatConsumer.listener.ChatBoardListener;
import com.lokalchat.LokalChatConsumer.managers.DataBaseManager;
import com.lokalchat.LokalChatConsumer.models.ChatBoardModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.FaqModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.models.entities.AnswerEntity;
import com.lokalchat.LokalChatConsumer.models.entities.ProductEntity;
import com.lokalchat.LokalChatConsumer.models.entities.QuestionEntity;
import com.lokalchat.LokalChatConsumer.models.entities.RetailerEntity;
import com.lokalchat.LokalChatConsumer.task.ChatBoardTask;
import com.lokalchat.LokalChatConsumer.utils.Utils;
import com.lokalchat.LokalChatConsumer.views.CustomProgress;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by Sudipta on 9/15/2015.
 */
public class ChatBoardActivity extends BaseActivity {
    public static String PRODUCT = "product";
    public static String PRODUCT_ID = "product_id";
    public static String QUESTION_TEXT = "question_text";
    public static String CHAT_ID = "chat_id";
    private EditText edtChatContent;
    private List<ChatBoardModel> chatBoardListItems;
    private ChatBoardListAdapter chatBoardListAdapter;
    private ProductModel productModel;
    private static Context callingActivity;
    private String productId;
    private String chatId;
    private ListView listViewChatBoard;
    private LinearLayout llSendChat;
    private boolean isFromInbox;
    private String questionText;
    private boolean hasActivityDestroyed;

    public static Intent getIntent(Context context, ProductModel productModel) {
        Intent intent = new Intent(context, ChatBoardActivity.class);
        intent.putExtra(PRODUCT, productModel);
        callingActivity = context;

        return intent;
    }

    public static Intent getIntent(Context context, String productId, String questionText, String chatId) {
        Intent intent = new Intent(context, ChatBoardActivity.class);
        intent.putExtra(PRODUCT_ID, productId);
        intent.putExtra(QUESTION_TEXT, questionText);
        intent.putExtra(CHAT_ID, chatId);
        callingActivity = context;

        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_board);

        productModel = this.getIntent().getParcelableExtra(PRODUCT);


        chatBoardListItems = new ArrayList<ChatBoardModel>();

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasActivityDestroyed = false;
    }

    private void initViews() {

        listViewChatBoard = (ListView) findViewById(R.id.lv_chat_board);
        final Button btnSendChat = (Button) findViewById(R.id.btn_send_chat);
        edtChatContent = (EditText) findViewById(R.id.edt_chat_content);
        llSendChat = (LinearLayout) findViewById(R.id.ll_send_chat);
        btnSendChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtChatContent.getText())) {
                    sendChat();
                } else {
                    Utils.showGenericDialog(ChatBoardActivity.this, R.string.enter_text, isActivityDestroyed());
                }

            }
        });

        if (callingActivity instanceof LandingActivity) {
            this.productId = this.getIntent().getStringExtra(PRODUCT_ID);
            this.questionText = this.getIntent().getStringExtra(QUESTION_TEXT);
            this.chatId = this.getIntent().getStringExtra(CHAT_ID);
            createChatBoardRowFromInbox();
            llSendChat.setVisibility(View.GONE);
            isFromInbox = true;
        } else {
            isFromInbox = false;
        }

        chatBoardListAdapter = new ChatBoardListAdapter(this, chatBoardListItems, isFromInbox);
        listViewChatBoard.setAdapter(chatBoardListAdapter);

        final Button btnFaq = (Button) findViewById(R.id.btn_faq);
        btnFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFaqDialog();
            }
        });

        if (isFromInbox) {
            setUpSupportActionBar(false, true, productModel.getProductName());
        } else {
            setUpSupportActionBar(false, true, getString(R.string.write_query));
        }


    }

    private void createChatBoardRowFromInbox() {

       /* MultiMap<String, ProductModel> chatMap = Utils.getShopChatApplication(this).getChatMap();

        List<ProductModel> chatList = (List<ProductModel>) chatMap.get(productId);*/


        List<ProductModel> chatList = Utils.getShopChatApplication(this).getChatList();

        for (ProductModel productModel : chatList) {
            if (productModel.getConsumerChatId().equalsIgnoreCase(chatId)) {
                this.productModel = productModel;
            }
        }


        ChatBoardModel chatBoardModel = new ChatBoardModel(productModel.getConsumerChatContent(), true, 0, null, productModel, productModel.getChatTimeStamp());
        chatBoardListItems.add(chatBoardModel);
        int retailerCount = 1;
        for (RetailerModel retailerModel : productModel.getRetailerModels()) {
            ChatBoardModel chatBoardModelRetailer = new ChatBoardModel(retailerModel.getRetailerChatContent(), false, retailerCount, retailerModel, productModel, retailerModel.getChatTimeStamp());
            chatBoardListItems.add(chatBoardModelRetailer);
            retailerCount++;
        }
    }

    private void showFaqDialog() {
        Dialog faqDialog = new Dialog(this);
        faqDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        faqDialog.setContentView(R.layout.dialog_faq);
        faqDialog.show();

        final ListView lvFaq = (ListView) faqDialog.findViewById(R.id.lv_faq);

        List<FaqModel> listFaq = new ArrayList<FaqModel>();
        listFaq.add(new FaqModel("Ask your question"));
        listFaq.add(new FaqModel("Check Product Price"));
        listFaq.add(new FaqModel("Check Product Availability"));
        listFaq.add(new FaqModel("Ask exact Shop Location"));
        listFaq.add(new FaqModel("Book Appointment/Service"));
        listFaq.add(new FaqModel("Provide feedback to Shop/Service"));


        FaqAdapter faqAdapter = new FaqAdapter(this, listFaq);
        lvFaq.setAdapter(faqAdapter);

    }

    private void sendChat() {

        Utils.getShopChatApplication(this).setChatBoardModels(createChatBoardRow());

        final CustomProgress progressDialog = Utils.getProgressDialog(this);
        ChatBoardListener chatBoardListener = new ChatBoardListener() {
            @Override
            public void onSendChatStart() {
                progressDialog.show();
            }

            @Override
            public void onSendChatSuccess(ChatBoardModel chatBoardModel) {
                progressDialog.dismiss();
                chatBoardListAdapter.notifyDataSetChanged();
                callLandingActivity(false);
            }

            @Override
            public void onSendChatFailure(ErrorModel errorModel) {
                insertUnsentChat();
                progressDialog.dismiss();
                chatBoardListAdapter.notifyDataSetChanged();
                Utils.hideKeyBoard(ChatBoardActivity.this, edtChatContent);
                showOfflineMessageDialog();
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        //Utils.showNetworkDisableDialog(ChatBoardActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(ChatBoardActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        //rollBackChat();
                        break;
                }

            }
        };

        ChatBoardTask chatBoardTask = new ChatBoardTask(this, chatBoardListener, false);
        chatBoardTask.sendChat();
    }

    private void showOfflineMessageDialog() {

        new AlertDialog.Builder(this)
                .setMessage(R.string.offline_message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        callLandingActivity(true);
                    }
                })

                .show();
    }

    private void insertUnsentChat() {
        List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication(this).getChatBoardModels();

        ChatBoardModel customerChatBoardModel = null;
        for (ChatBoardModel chatBoardModel_ : chatBoardList) {
            if (chatBoardModel_.isCustomer()) {
                customerChatBoardModel = chatBoardModel_;
            }
        }


        // Storing chat in DB
        DataBaseManager dataBaseManager = new DataBaseManager(this);
        Random random = new Random();
        int randomQuestionId = random.nextInt(80 - 65) + 65;
        customerChatBoardModel.setConsumerChatId(String.valueOf(randomQuestionId));
        QuestionEntity questionEntity = new QuestionEntity(customerChatBoardModel.getConsumerChatId(),
                customerChatBoardModel.getProductModel().getProductId(), customerChatBoardModel.getChatContent(), 0, String.valueOf(System.currentTimeMillis()), 0);
        dataBaseManager.addQuestionEntity(questionEntity);
        ProductEntity productEntity = new ProductEntity(customerChatBoardModel.getProductModel().getProductId(), "", "",
                customerChatBoardModel.getProductModel().getProductName(), "", "");
        dataBaseManager.addProductEntity(productEntity);

        List<AnswerEntity> answerEntityList = new ArrayList<AnswerEntity>();
        List<RetailerEntity> retailerEntityList = new ArrayList<RetailerEntity>();

        List<RetailerModel> selectedRetailerList = ((LokalChatApplication) getApplication()).getRetailerList();
        for (RetailerModel retailerModel : selectedRetailerList) {
            int randomId = random.nextInt(800 - 650) + 650;
            retailerModel.setAnswerId(String.valueOf(randomId));
            AnswerEntity answerEntity = new AnswerEntity(retailerModel.getAnswerId(), retailerModel.getRetailerId(),
                    questionEntity.getQuestionId(),"", String.valueOf(System.currentTimeMillis()));
            RetailerEntity retailerEntity = new RetailerEntity(retailerModel.getRetailerId(), retailerModel.getRetailerName());

            answerEntityList.add(answerEntity);
            retailerEntityList.add(retailerEntity);
        }

        dataBaseManager.addAnswerEntities((ArrayList<AnswerEntity>) answerEntityList);
        dataBaseManager.addRetailerEntities((ArrayList<RetailerEntity>) retailerEntityList);

        customerChatBoardModel.setRetailerModelList(selectedRetailerList);
        Utils.getShopChatApplication(this).getUnsentChatList().add(customerChatBoardModel);

    }

    private void callLandingActivity(boolean isUnSentPool) {
        startActivity(LandingActivity.getIntent(this, ChatBoardActivity.class.toString(), productModel, isUnSentPool));
    }

    private void rollBackChat() {
        // Last item of List
        chatBoardListItems.remove(chatBoardListItems.size() - 1);
        Utils.getShopChatApplication(ChatBoardActivity.this).getChatBoardModels().clear();
        chatBoardListAdapter.notifyDataSetChanged();

        // TODO Localization
        Toast.makeText(ChatBoardActivity.this, "Sending failed!", Toast.LENGTH_SHORT).show();
    }

    private List<ChatBoardModel> createChatBoardRow() {

        ChatBoardModel chatBoardModel = new ChatBoardModel(edtChatContent.getText().toString(), true, 0, null, productModel, null);
        chatBoardListItems.add(chatBoardModel);
        edtChatContent.setText("");
        //Utils.hideKeyBoard(ChatBoardActivity.this, edtChatContent);
        List<RetailerModel> selectedRetailerList = new ArrayList<>();
        if (callingActivity instanceof LandingActivity) {
            selectedRetailerList = getSelectedRetailerList();
        } else {
            selectedRetailerList = ((LokalChatApplication) getApplication()).getRetailerList();
        }

        int retailerCount = 1;
        for (RetailerModel retailerModel : selectedRetailerList) {
            ChatBoardModel chatBoardModelRetailer = new ChatBoardModel(getResources().getString(R.string.reply_pending), false,
                    retailerCount, retailerModel, productModel, null);
            chatBoardListItems.add(chatBoardModelRetailer);
            retailerCount++;
        }

        return chatBoardListItems;
    }

    private List<RetailerModel> getSelectedRetailerList() {
        List<RetailerModel> selectedRetailerList = new ArrayList<>();
        MultiMap<String, ProductModel> chatMap = Utils.getShopChatApplication(this).getChatMap();

        List<ProductModel> chatList = (List<ProductModel>) chatMap.get(productId);

        MultiMap<String, RetailerModel> retailerMap = new MultiValueMap<>();
        for (ProductModel productModel : chatList) {

            for (RetailerModel retailerModel : productModel.getRetailerModels()) {
                retailerMap.put(retailerModel.getRetailerId(), retailerModel);
            }
        }

        Iterator iterator = retailerMap.entrySet().iterator();

        while (iterator.hasNext()) {
            MultiValueMap.Entry entry = (MultiValueMap.Entry) iterator.next();
            ArrayList<RetailerModel> retailerModelList = (ArrayList) entry.getValue();
            if (retailerModelList != null && !retailerModelList.isEmpty()) {
                selectedRetailerList.add(retailerModelList.get(0));
            }
        }

        return selectedRetailerList;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }
}
