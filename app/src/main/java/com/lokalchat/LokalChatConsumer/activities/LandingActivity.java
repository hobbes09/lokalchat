package com.lokalchat.LokalChatConsumer.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.adapters.HomePageFragmentAdapter;
import com.lokalchat.LokalChatConsumer.fragments.ChatFragment;
import com.lokalchat.LokalChatConsumer.fragments.HomeFragment;
import com.lokalchat.LokalChatConsumer.fragments.LoakalChatNavigationFragment;
import com.lokalchat.LokalChatConsumer.fragments.ProfileFragment;
import com.lokalchat.LokalChatConsumer.listener.ChatBoardListener;
import com.lokalchat.LokalChatConsumer.listener.ChatListener;
import com.lokalchat.LokalChatConsumer.listener.CityListener;
import com.lokalchat.LokalChatConsumer.listener.NewMessageCountListener;
import com.lokalchat.LokalChatConsumer.managers.DataBaseManager;
import com.lokalchat.LokalChatConsumer.models.ChatBoardModel;
import com.lokalchat.LokalChatConsumer.models.ChatDisplayModel;
import com.lokalchat.LokalChatConsumer.models.CityModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.models.entities.AnswerEntity;
import com.lokalchat.LokalChatConsumer.models.entities.QuestionEntity;
import com.lokalchat.LokalChatConsumer.task.ChatBoardTask;
import com.lokalchat.LokalChatConsumer.task.ChatTask;
import com.lokalchat.LokalChatConsumer.task.CityTask;
import com.lokalchat.LokalChatConsumer.task.NewMessageCountTask;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;
import com.lokalchat.LokalChatConsumer.views.ActionBarHome;
import com.lokalchat.LokalChatConsumer.views.CustomProgress;
import com.lokalchat.LokalChatConsumer.views.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sudipta on 8/7/2015.
 */
public class LandingActivity extends BaseActivity implements ActionBarHome.OnActionBarItemClickListener {

    /**
     * The absence of a connection type.
     */
    private static final int NO_CONNECTION_TYPE = -1;
    private static final int REFRESH_TIME = 60000;
    public static String CALLING_ACTIVITY = "calling_activity";
    public static String PRODUCT = "product";
    public static String PRODUCT_ID = "product_id";
    /**
     * The last processed network type.
     */
    private static int sLastType = NO_CONNECTION_TYPE;
    private static String ISUNSENTPOOL = "is_unsent_pool";
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private LoakalChatNavigationFragment mNavigationDrawerFragment;
    private HomePageFragmentAdapter adapter;
    private SaveClickListener saveClickListener;
    private ActionBarHome actionBarHome;
    private CustomProgress progressDialog;
    private ChatAdapterListener chatAdapterListener;
    private ProductModel productModel;
    private Timer timer;
    private boolean hasActivityDestroyed;
    private String callingActivity;
    private boolean isChatFirstTimeLoad = true;
    private NetworkReceiver networkReceiver;
    private boolean isUnsentPool;
    ChatFragment.PageListener pageListener = new ChatFragment.PageListener() {
        @Override
        public void onLoadMore(int limit) {
            int currentPageNumber = Utils.getShopChatApplication(LandingActivity.this).getCurrentChatPageNumber() + 1;
            initChatFetchTask(String.valueOf(currentPageNumber), true, false, limit);
        }
    };

    public static Intent getIntent(Context context, String callingActivity, ProductModel productModel, boolean isUnSentPool) {
        Intent intent = new Intent(context, LandingActivity.class);
        intent.putExtra(CALLING_ACTIVITY, callingActivity);
        intent.putExtra(PRODUCT, productModel);
        intent.putExtra(ISUNSENTPOOL, isUnSentPool);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setUpSupportActionBar(true, false, "Home");

        mNavigationDrawerFragment = (LoakalChatNavigationFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
        mNavigationDrawerFragment.closeNavigationDrawer();

        isUnsentPool = this.getIntent().getBooleanExtra(ISUNSENTPOOL, false);

        initViews();

        if (callingActivity == null) {
            initChatFetchTask("0", false, false, 5);
        }

        networkReceiver = new NetworkReceiver(new NetworkListener() {
            @Override
            public void onNetworkChanged() {

                if (Utils.isConnectionPossible(LandingActivity.this)) {
                    handleMultipleConnectionReceiver();
                    Log.d(Constants.TAG, "Network Received");
                }
            }
        });

        registerReceiver(networkReceiver, new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION));

        /**
         * Check it there is any unsent messages are available to post
         */
        if (Utils.isConnectionPossible(this)) {
            postUnsentChat();
        }

    }

    private void handleMultipleConnectionReceiver() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        final int currentType = activeNetworkInfo != null
                ? activeNetworkInfo.getType() : NO_CONNECTION_TYPE;

        // Avoid handling multiple broadcasts for the same connection type
        if (sLastType != currentType) {
            if (activeNetworkInfo != null) {
                Log.d(Constants.TAG, "Your are now online");
                postUnsentChat();
            } else {

            }

            sLastType = currentType;
        }
    }

    private void postUnsentChat() {

        isUnsentPool = false;

        ChatBoardListener chatBoardListener = new ChatBoardListener() {
            @Override
            public void onSendChatStart() {

            }

            @Override
            public void onSendChatSuccess(ChatBoardModel chatBoardModel) {

                Utils.getShopChatApplication(LandingActivity.this).getUnsentChatList().remove(chatBoardModel);

                /**
                 * Remove from local DB to avoid duplicate model, because same model data will be retrieve from server.
                 */
                removeFromLocalDB(chatBoardModel);
                isChatFirstTimeLoad = true;
                initChatFetchTask("0", false, false, 5);
                postUnsentChat();
            }

            @Override
            public void onSendChatFailure(ErrorModel errorModel) {

            }
        };
        List<ChatBoardModel> unsentChatList = Utils.getShopChatApplication(LandingActivity.this).getUnsentChatList();
        if (unsentChatList != null && unsentChatList.size() != 0) {
            ChatBoardTask chatBoardTask = new ChatBoardTask(this, chatBoardListener, true);
            chatBoardTask.sendChat();
        }
    }

    private void removeFromLocalDB(ChatBoardModel chatBoardModel) {
        DataBaseManager dataBaseManager = new DataBaseManager(this);
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setQuestionId(chatBoardModel.getConsumerChatId());
        dataBaseManager.deleteQuestion(questionEntity);

        for (RetailerModel retailerModel : chatBoardModel.getRetailerModelList()) {
            AnswerEntity answerEntity = new AnswerEntity();
            answerEntity.setAnswerId(retailerModel.getAnswerId());
            dataBaseManager.deleteAnswer(answerEntity);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        actionBarHome = getActionBarHome();
        hasActivityDestroyed = false;
        setUpSelectedLocation();

        fetchChatPeriodically();
    }

    private void fetchChatPeriodically() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initNewMessageTask();
                    }
                });
            }
        }, 0, REFRESH_TIME);
    }

    private void initNewMessageTask() {

        NewMessageCountListener newMessageCountListener = new NewMessageCountListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(String response) {
                if (response.equalsIgnoreCase("0")) {

                } else {
                    if (viewPager.getCurrentItem() == 0) {
                        initChatFetchTask("0", false, true, 5);
                    } else {
                        initChatFetchTask("0", false, false, 5);
                    }
                }
            }

            @Override
            public void onFailure(ErrorModel errorModel) {
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        //Utils.showNetworkDisableDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:

                        break;
                }
            }
        };

        NewMessageCountTask newMessageCountTask = new NewMessageCountTask(this, newMessageCountListener);
        newMessageCountTask.fetchNewMessage();
    }

    private void initChatFetchTask(final String pageCounter, final boolean isLoadMore, final boolean isFromAlertDialog, int limit) {

        progressDialog = Utils.getProgressDialog(this);
        ChatListener chatListener = new ChatListener() {
            @Override
            public void onChatFetchStart() {
                // progressDialog.show();
            }

            @Override
            public void onChatFetchSuccess(List<ProductModel> chatList) {
                //progressDialog.dismiss();
                isChatFirstTimeLoad = false;
                List<ChatDisplayModel> chatDisplayModelList = createInboxDisplayList(chatList);


                if (!isLoadMore) {
                    if (isFromAlertDialog) {
                        // TODO Localization
                        if (!isActivityDestroyed()) {
                            showNewMessageAvailableDialog(LandingActivity.this, "There is a new message available, would you like to read it?", chatList, chatDisplayModelList);
                        } else {
                            setChatData(chatList, chatDisplayModelList);
                            chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);
                        }
                    } else {
                        setChatData(chatList, chatDisplayModelList);
                        chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);
                    }

                } else {
                    setChatData(chatList, chatDisplayModelList);
                    chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);
                }
            }

            @Override
            public void onChatFetchFailure(ErrorModel errorModel) {
                // progressDialog.dismiss();
                chatAdapterListener.onChatFetchFailure(errorModel);
            }
        };


        ChatTask chatTask = new ChatTask(this, chatListener, pageCounter, isLoadMore, isChatFirstTimeLoad, isFromAlertDialog, limit, isUnsentPool);
        chatTask.fetchChat();
    }

    private void addChatData(List<ProductModel> chatList, List<ChatDisplayModel> chatDisplayModelList) {
        Utils.getShopChatApplication(LandingActivity.this).getChatDisplayModels().addAll(chatDisplayModelList);
        Utils.getShopChatApplication(LandingActivity.this).getChatList().addAll(chatList);
    }

    private void setChatData(List<ProductModel> chatList, List<ChatDisplayModel> chatDisplayModelList) {
        Utils.getShopChatApplication(LandingActivity.this).setChatDisplayModels(chatDisplayModelList);
        Utils.getShopChatApplication(LandingActivity.this).setChatList(chatList);
    }

    private void storeAndDisplayChat(List<ChatDisplayModel> chatDisplayModelList) {
        List<ChatDisplayModel> cachedChatDisplayList = Utils.getShopChatApplication(LandingActivity.this).getChatDisplayModels();

        if (cachedChatDisplayList != null && !cachedChatDisplayList.isEmpty()) {

            for (ChatDisplayModel cachedChatDisplayModel : cachedChatDisplayList) {
                String replyContent = cachedChatDisplayModel.getRetailerConsolidatedChatContent();
                String productId = cachedChatDisplayModel.getProductId();

                for (ChatDisplayModel chatDisplayModel : chatDisplayModelList) {
                    if (productId.equalsIgnoreCase(chatDisplayModel.getProductId())) {
                        if (!replyContent.equalsIgnoreCase(chatDisplayModel.getRetailerConsolidatedChatContent())) {
                            chatDisplayModel.setIsRead(true);
                        }
                    }
                }

            }
        }
        Utils.getShopChatApplication(LandingActivity.this).setChatDisplayModels(chatDisplayModelList);
        chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);

    }

    /**
     * Create DisplayModel for inbox view
     *
     * @param chatList
     * @return
     */
    private List<ChatDisplayModel> createInboxDisplayList(List<ProductModel> chatList) {
        List<ChatDisplayModel> chatDisplayModelList = new ArrayList<ChatDisplayModel>();


        for (ProductModel productModel : chatList) {
            ChatDisplayModel chatDisplayModel = new ChatDisplayModel();
            chatDisplayModel.setChatId(productModel.getConsumerChatId());
            chatDisplayModel.setProductId(productModel.getProductId());
            chatDisplayModel.setProductName(productModel.getProductName());
            StringBuilder stringBuilder = new StringBuilder();
            for (RetailerModel retailerModel : productModel.getRetailerModels()) {
                if (!retailerModel.getRetailerChatContent().isEmpty()) {
                    stringBuilder.append(retailerModel.getRetailerChatContent());
                    stringBuilder.append(" ");
                }
            }
            chatDisplayModel.setConsumerChatContent(productModel.getConsumerChatContent());
            chatDisplayModel.setRetailerConsolidatedChatContent(stringBuilder.toString());
            chatDisplayModel.setIsRead(productModel.isRead());
            chatDisplayModel.setIsSent(productModel.isSent());
            chatDisplayModelList.add(chatDisplayModel);
        }


        return chatDisplayModelList;
    }


    private void setUpSelectedLocation() {
        final String selectedCity = Utils.getPersistenceData(this, Constants.CITY_PREFERENCE);
        final String selectedLocation = Utils.getPersistenceData(this, Constants.LOCATION_PREFERENCE);
        if (TextUtils.isEmpty(selectedCity) || TextUtils.isEmpty(selectedLocation)) {
            showPickCityDialog();
        } else {
            getActionBarHome().setSelectedState(selectedCity);
            getActionBarHome().setSelectedLocation(selectedLocation);
        }
    }

    private void initViews() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            adapter = new HomePageFragmentAdapter(getFragmentManager());
        } else {
            adapter = new HomePageFragmentAdapter(getFragmentManager());
            adapter.setReuseFragments(false);
        }

        ChatFragment chatFragment = new ChatFragment();
        chatFragment.setPageListener(pageListener);

        adapter.addFragment(getString(R.string.chat_fragment), chatFragment);
        adapter.addFragment(getString(R.string.home_fragment), new HomeFragment());
        adapter.addFragment(getString(R.string.profile_fragment), new ProfileFragment());

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);

        viewPager.setAdapter(adapter);
        slidingTabLayout.setViewPager(viewPager);

        callingActivity = this.getIntent().getStringExtra(CALLING_ACTIVITY);
        productModel = this.getIntent().getParcelableExtra(PRODUCT);
        if (callingActivity != null && callingActivity.equalsIgnoreCase(ChatBoardActivity.class.toString()) && productModel != null) {
            viewPager.setCurrentItem(0);
            initChatFetchTask("0", false, false, 5);
        } else {
            viewPager.setCurrentItem(1);
        }

        slidingTabLayout.addPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (adapter.getItem(position) instanceof ProfileFragment) {
                   /* changeActionBarMultipleMenuIcon(R.drawable.ic_arrow_back_white_24dp);
                    getActionBarHome().hideActionBarMultipleMenuIcon();*/
                    getActionBarHome().setVisibleFragment(new ProfileFragment());
                } else {
                    changeActionBarMultipleMenuIcon(R.drawable.ic_share);
                    getActionBarHome().showActionBarMultipleMenuIcon();
                    getActionBarHome().setVisibleFragment(null);
                }

                if (adapter.getItem(position) instanceof ChatFragment) {
                    //initChatFetchTask();
                }

                Utils.hideKeyBoard(LandingActivity.this);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onSelectedLocationClick() {
        initCityFetch();
    }

    @Override
    public void onMultipleActionMenuClick() {
        if (adapter.getItem(viewPager.getCurrentItem()) instanceof ProfileFragment) {
            saveClickListener.onSaveClick();
        } else {
            // TODO Replace with actual data
            Utils.callShareIntent(this, getString(R.string.app_name), Utils.getSharableContent(this));
        }
    }


    @Override
    public void onMenuIconClick() {
        mNavigationDrawerFragment.openNavigationDrawer();
    }

    public void setSaveClickListener(SaveClickListener listener) {
        this.saveClickListener = listener;
    }

    public void setChatAdapterListener(ChatAdapterListener chatAdapterListener) {
        this.chatAdapterListener = chatAdapterListener;
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isVisible()) {
            mNavigationDrawerFragment.closeNavigationDrawer();
        } else {
            Utils.showExitConfirmationDialog(this);
        }
    }

    public ActionBarHome getActivityActionBar() {
        return actionBarHome;
    }

    public void showPickCityDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.please_pick_location)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        initCityFetch();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        closeActivity();
                    }
                })
                .show();
    }

    private void initCityFetch() {

        progressDialog = Utils.getProgressDialog(this);
        CityListener cityListener = new CityListener() {
            @Override
            public void onCityFetchedStart() {
                progressDialog.show();
            }

            @Override
            public void onCityFetchSuccess(List<CityModel> cityModelList) {
                progressDialog.dismiss();
                Utils.getShopChatApplication(LandingActivity.this).setCityModelList(cityModelList);
                startActivity(CityActivity.getIntent(LandingActivity.this));
            }

            @Override
            public void onCityFetchFailure(ErrorModel errorModel) {
                progressDialog.dismiss();

                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        break;
                }

            }
        };

        CityTask cityTask = new CityTask(this, cityListener);
        cityTask.fetchCity();
    }

    private void closeActivity() {
        this.finish();
    }

    public ProductModel getProductModel() {
        return this.productModel;
    }

    public CustomProgress getProgressDialog() {
        return progressDialog;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
        isChatFirstTimeLoad = false;
        Constants.SELECTED_POSITION = -1;
        Utils.getShopChatApplication(this).getChatDisplayModels().clear();
        unregisterReceiver(networkReceiver);
    }

    public void showNewMessageAvailableDialog(final Context context, String msg, final List<ProductModel> chatList, final List<ChatDisplayModel> chatDisplayModelList) {
        new AlertDialog.Builder(context)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setChatData(chatList, chatDisplayModelList);
                        chatAdapterListener.onChatFetchSuccess(Utils.getShopChatApplication(LandingActivity.this).getChatDisplayModels());
                    }
                })
                .show();
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    public interface SaveClickListener {
        void onSaveClick();
    }

    public interface ChatAdapterListener {
        void onChatFetchSuccess(List<ChatDisplayModel> chatDisplayModels);

        void onChatFetchFailure(ErrorModel errorModel);
    }

    private interface NetworkListener {
        void onNetworkChanged();
    }

    public static class NetworkReceiver extends BroadcastReceiver {
        private final NetworkListener listener;

        public NetworkReceiver(NetworkListener listener) {
            this.listener = listener;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != listener) {
                listener.onNetworkChanged();
            }
        }
    }
}
