package com.lokalchat.LokalChatConsumer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.listener.RegistrationListener;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.RegistrationModel;
import com.lokalchat.LokalChatConsumer.task.RegistrationTask;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;
import com.lokalchat.LokalChatConsumer.views.CustomProgress;

/**
 * Created by Sudipta on 9/5/2015.
 */
public class SignUpActivity extends BaseActivity {

    public static final String PHONE_NUMBER = "phone_number";
    private EditText editTextName;
    private EditText editTextEmail;
    private EditText editTextPhone;
    private String phoneNumber;
    private CustomProgress progressDialog;
    private boolean hasActivityDestroyed;
    private EditText edtNewPassword;
    private EditText edtConfirmPassword;


    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, SignUpActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setUpSupportActionBar(false, false, getString(R.string.sign_up));
        changeHamburgerIcon(R.drawable.ic_launcher);

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasActivityDestroyed = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    private void initViews() {

        editTextName = (EditText) findViewById(R.id.edt_name);
        editTextPhone = (EditText) findViewById(R.id.edt_phone);


        editTextEmail = (EditText) findViewById(R.id.edt_email);

       /* edtNewPassword = (EditText) findViewById(R.id.edt_new_password);
        edtConfirmPassword = (EditText) findViewById(R.id.edt_confirm_password);*/

        final Button btnSignUp = (Button) findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasPhoneNumberValidated() && hasEmailValidated() ) {
                    initRegistrationTask();
                }
            }
        });
    }

    private boolean hasPasswordValidated() {

        boolean hasValidated = false;

        if (edtNewPassword.length() == 0) {
            Utils.showGenericDialog(this, R.string.enter_new_password, isActivityDestroyed());
        } else if (edtConfirmPassword.length() == 0) {
            Utils.showGenericDialog(this, R.string.enter_confirm_password, isActivityDestroyed());
        } else if (!edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            Utils.showGenericDialog(this, R.string.password_not_match, isActivityDestroyed());
        } else {
            hasValidated = true;
        }

        return hasValidated;
    }


    private void initRegistrationTask() {

        progressDialog = Utils.getProgressDialog(this);

        RegistrationListener registrationListener = new RegistrationListener() {
            @Override
            public void onRegistrationStart() {
                progressDialog.show();
            }

            @Override
            public void onRegistrationSuccess() {
                progressDialog.dismiss();
                saveDataInPreference();
                callOTPActivity();
            }

            @Override
            public void onRegistrationFailure(ErrorModel errorModel) {
                progressDialog.dismiss();
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(SignUpActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(SignUpActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_BAD_REQUEST:
                        Utils.showGenericDialog(SignUpActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                    case ERROR_TYPE_CONFLICT:
                        saveDataInPreference();
                        callOTPActivity();
                        break;
                    default:
                        break;
                }
            }
        };

        RegistrationModel registrationModel = new RegistrationModel();
        registrationModel.setName(editTextName.getText().toString());
        registrationModel.setPhoneNumber(editTextPhone.getText().toString());
        registrationModel.setEmail(editTextEmail.getText().toString());
        RegistrationTask registrationTask = new RegistrationTask(this, registrationListener, registrationModel);
        registrationTask.initRegistration();

    }

    private void callSplashActivity() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        this.finish();
    }

    private void callOTPActivity() {
        Intent intent = new Intent(this, OTPActivity.class);
        startActivity(intent);
        this.finish();
    }

    private boolean hasPhoneNumberValidated() {
        boolean hasValidated;

        if (TextUtils.isEmpty(editTextPhone.getText())) {
            hasValidated = false;
            Utils.showGenericDialog(this, R.string.phone_number_mandatory, isActivityDestroyed());
        } else if (editTextPhone.length() != 10) {
            hasValidated = false;
            Utils.showGenericDialog(this, R.string.phone_digit_validation, isActivityDestroyed());
        } else {
            hasValidated = true;
        }

        return hasValidated;
    }

    private boolean hasEmailValidated() {
        boolean hasValidated = false;

        if (TextUtils.isEmpty(editTextEmail.getText())) {
            hasValidated = true;
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText()).matches()) {
                hasValidated = true;
            } else {
                Utils.showGenericDialog(this, R.string.invalid_email, isActivityDestroyed());
            }
        }

        return hasValidated;
    }


    private void saveDataInPreference() {
        Utils.setPersistenceData(this, Constants.REGISTERED_NAME_PREFERENCE, editTextName.getText().toString());

        Utils.setPersistenceData(this, Constants.REGISTERED_EMAIL_PREFERENCE, editTextEmail.getText().toString());

        Utils.setPersistenceData(this, Constants.REGISTERED_PHONE_PREFERENCE, editTextPhone.getText().toString());

        Utils.setPersistenceBoolean(this, Constants.IS_REGISTERED_PREFERENCE, true);

        /*String registeredPhone = Utils.getPersistenceData(this, Constants.REGISTERED_PHONE_PREFERENCE);
        if (!TextUtils.isEmpty(registeredPhone)) {
            Utils.setPersistenceData(this, registeredPhone, edtConfirmPassword.getText().toString());
        }*/
    }

    @Override
    public void onBackPressed() {
        Utils.showExitConfirmationDialog(this);
    }
}
