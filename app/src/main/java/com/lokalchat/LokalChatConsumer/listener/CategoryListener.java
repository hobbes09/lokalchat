package com.lokalchat.LokalChatConsumer.listener;

import com.lokalchat.LokalChatConsumer.models.CategoryModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;

import java.util.List;

/**
 * Created by Sudipta on 9/24/2015.
 */
public abstract class CategoryListener {
    public abstract void onCategoryFetchStart();

    public abstract void onCategoryFetchSuccess(List<CategoryModel> categoryModelList);

    public abstract void onCategoryFetchFailure(ErrorModel errorModel);
}
