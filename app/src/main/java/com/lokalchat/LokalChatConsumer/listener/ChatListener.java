package com.lokalchat.LokalChatConsumer.listener;

import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;

import java.util.List;

/**
 * Created by Sudipta on 9/26/2015.
 */
public abstract class ChatListener {

    public abstract void onChatFetchStart();

    //public abstract void onChatFetchSuccess(MultiMap<String, ProductModel> chatMap);
    public abstract void onChatFetchSuccess(List<ProductModel> chatList);

    public abstract void onChatFetchFailure(ErrorModel errorModel);
}

