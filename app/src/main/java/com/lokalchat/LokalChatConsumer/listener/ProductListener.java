package com.lokalchat.LokalChatConsumer.listener;

import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;

import java.util.List;

/**
 * Created by Sudipta on 9/26/2015.
 */
public abstract class ProductListener {

    public abstract void onProductFetchStart();

    public abstract void onProductFetchSuccess(List<ProductModel> productModels);

    public abstract void onProductFetchFailure(ErrorModel errorModel);
}
