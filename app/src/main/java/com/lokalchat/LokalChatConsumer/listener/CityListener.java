package com.lokalchat.LokalChatConsumer.listener;

import com.lokalchat.LokalChatConsumer.models.CityModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;

import java.util.List;

/**
 * Created by Sudipta on 9/25/2015.
 */
public abstract class CityListener {

    public abstract void onCityFetchedStart();

    public abstract void onCityFetchSuccess(List<CityModel> cityModelList);

    public abstract void onCityFetchFailure(ErrorModel errorModel);

}
