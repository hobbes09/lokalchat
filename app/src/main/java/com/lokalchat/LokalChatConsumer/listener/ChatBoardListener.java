package com.lokalchat.LokalChatConsumer.listener;


import com.lokalchat.LokalChatConsumer.models.ChatBoardModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;

/**
 * Created by Sudipta on 9/27/2015.
 */
public abstract class ChatBoardListener {

    public abstract void onSendChatStart();

    public abstract void onSendChatSuccess(ChatBoardModel chatBoardModel);

    public abstract void onSendChatFailure(ErrorModel errorModel);
}
