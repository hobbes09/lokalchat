
package com.lokalchat.LokalChatConsumer.listener;


import com.lokalchat.LokalChatConsumer.models.ErrorModel;

public abstract class AuthenticationListener {

    //public abstract void onAuthenticationStart(CustomProgress progressDialog);*/

    public abstract void onAuthenticationSuccess();

    public abstract void onAuthenticationFailure(ErrorModel errorModel);

}
