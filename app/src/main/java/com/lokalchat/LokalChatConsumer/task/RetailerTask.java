package com.lokalchat.LokalChatConsumer.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.listener.RetailerListener;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.LoginModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.network.HttpConnection;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;
import com.lokalchat.LokalChatConsumer.LokalChatApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sudipta on 9/26/2015.
 */
public class RetailerTask {
    private Context context;
    private HttpConnection httpConnect;
    private RetailerListener retailerListener;
    private ProductModel productModel;

    public RetailerTask(Context context, RetailerListener retailerListener, ProductModel productModel) {
        this.context = context;
        this.retailerListener = retailerListener;
        this.productModel = productModel;

    }

    public void fetchRetailer() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            retailerListener.onRetailerFetchFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        retailerListener.onRetailerFetchStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        retailerListener.onRetailerFetchSuccess(getRetailerList(jsonResponse));
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        retailerListener.onRetailerFetchFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        retailerListener.onRetailerFetchFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String retailerUrl = Constants.BASE_URL + Constants.RETAILER_URL;
        LoginModel loginModel = ((LokalChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(retailerUrl, null, getRequestBody(), HttpConnection.REQUEST_COMMON, loginModel);
    }

    private String getRequestBody() {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("productName", productModel.getProductName());
            final String selectedLocation = Utils.getPersistenceData(this.context, Constants.LOCATION_PREFERENCE);
            jsonObject.put("locality", selectedLocation);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }

    private List<RetailerModel> getRetailerList(String response) {
        List<RetailerModel> retailerModels = new ArrayList<RetailerModel>();

        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int ii = 0; ii < jsonArray.length(); ii++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(ii);
                RetailerModel retailerModel = new RetailerModel();
                retailerModel.setRetailerId(jsonObject.getString("id"));
                retailerModel.setRetailerName(jsonObject.getString("shopName"));
                retailerModels.add(retailerModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return retailerModels;
    }
}
