
package com.lokalchat.LokalChatConsumer.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.lokalchat.LokalChatConsumer.LokalChatApplication;
import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.listener.AuthenticationListener;
import com.lokalchat.LokalChatConsumer.models.AuthenticationModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.LoginModel;
import com.lokalchat.LokalChatConsumer.network.HttpConnection;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


public class AuthenticationTask {
    private Context context;
    private HttpConnection httpConnect;
    private AuthenticationListener authenticationListener;
    private AuthenticationModel authenticationModel;

    public AuthenticationTask(Context context, AuthenticationListener listener,
                              AuthenticationModel authenticationModel) {
        this.context = context;
        this.authenticationListener = listener;
        this.authenticationModel = authenticationModel;
    }

    public void authenticate() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            authenticationListener.onAuthenticationFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:

                       /* authenticationListener.onAuthenticationStart(Utils
                                .getProgressDialog(context));*/
                        break;
                    case HttpConnection.DID_SUCCEED:
                        LoginModel loginModel = (LoginModel) message.obj;
                        ((LokalChatApplication) context.getApplicationContext()).setLoginModel(loginModel);
                        authenticationListener.onAuthenticationSuccess();
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        String msg = (String) message.obj;
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_UNAUTHORIZED);
                        errorModel.setErrorMessage(msg);
                        authenticationListener.onAuthenticationFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        authenticationListener.onAuthenticationFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("username", authenticationModel.getUserId()));
      /*  nameValuePairs.add(new BasicNameValuePair("username", "myuser1"));
        nameValuePairs.add(new BasicNameValuePair("password", "myuser1"));*/
        nameValuePairs.add(new BasicNameValuePair("password", authenticationModel.getPassword()));
        final String authenticationUrl = Constants.BASE_URL + Constants.LOGIN_URL;
        httpConnect.post(authenticationUrl, nameValuePairs, null, HttpConnection.REQUEST_LOGIN, new LoginModel());

    }

}
