package com.lokalchat.LokalChatConsumer.task;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.lokalchat.LokalChatConsumer.LokalChatApplication;
import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.listener.ChatListener;
import com.lokalchat.LokalChatConsumer.managers.DataBaseManager;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.LoginModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.models.entities.AnswerEntity;
import com.lokalchat.LokalChatConsumer.models.entities.ProductEntity;
import com.lokalchat.LokalChatConsumer.models.entities.QuestionEntity;
import com.lokalchat.LokalChatConsumer.models.entities.RetailerEntity;
import com.lokalchat.LokalChatConsumer.network.HttpConnection;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sudipta on 9/26/2015.
 */
public class ChatTask {

    private Activity context;
    private HttpConnection httpConnect;
    private ChatListener chatListener;
    private DataBaseManager dataBaseManager;
    private String pageCounter = "0";
    private boolean isFirstTime;
    private boolean isLoadMore;
    private int limit;
    private boolean isFromCounter; // isFromCounter = isFromAlertDialog
    private boolean isUnsentPool;

    public ChatTask(Activity context, ChatListener chatListener, String pageCounter,
                    boolean isLoadMore, boolean isFirstTime, boolean isFromCounter, int limit, boolean isUnsentPool) {
        this.context = context;
        this.chatListener = chatListener;
        this.pageCounter = pageCounter;
        this.isFirstTime = isFirstTime;
        this.isLoadMore = isLoadMore;
        this.limit = limit;
        this.isFromCounter = isFromCounter;
        this.isUnsentPool = isUnsentPool;
        this.dataBaseManager = new DataBaseManager(context);
    }

    public void fetchChat() {

        if (isUnsentPool) {
            onChatFragmentVisible();
            return;
        }

        if (!isFirstTime && !isLoadMore && !isFromCounter && this.dataBaseManager.getQuestionCount() > 0) {
            onChatFragmentVisible();
        } else if (isLoadMore) {
            int onlineRecords = Utils.getShopChatApplication(context).getTotalOnlineRecord();
            int dbRecords = this.dataBaseManager.getQuestionCount();

            if (dbRecords < onlineRecords) {
                fetchChatOnline();
            } else {
                fetchChatOffline();
            }
        } else {
            fetchChatOnline();
        }

    }

    /**
     * Wait fragment to visible till offline data fetch is done.
     */
    private void onChatFragmentVisible() {
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fetchChatOffline();
                    }
                });

                timer.cancel();
            }
        }, 500, 500);
    }

    public void fetchChatOffline() {
        List<QuestionEntity> questionEntityList = new ArrayList<QuestionEntity>();
        questionEntityList = this.dataBaseManager.getAllQuestionsOrderByTimeStamp(String.valueOf(limit));
        List<ProductModel> productModelList = new ArrayList<ProductModel>();
        for (QuestionEntity questionEntity : questionEntityList) {
            ProductEntity productEntity = this.dataBaseManager.getProductEntityFromProductId(questionEntity.getProductId());
            ProductModel productModel = new ProductModel();
            productModel.setConsumerChatId(questionEntity.getQuestionId());
            productModel.setConsumerChatContent(questionEntity.getQuestionText());
            productModel.setProductId(productEntity.getProductId());
            productModel.setProductName(productEntity.getProductName());
            productModel.setChatTimeStamp(questionEntity.getUpdatedAt());
            if (questionEntity.getSeen() > 0) {
                productModel.setIsRead(true);
            } else {
                productModel.setIsRead(false);
            }

            if (questionEntity.getSent() > 0) {
                productModel.setIsSent(true);
            } else {
                productModel.setIsSent(false);
            }

            List<AnswerEntity> answerEntityList = new ArrayList<AnswerEntity>();
            List<RetailerModel> retailerModelList = new ArrayList<RetailerModel>();
            answerEntityList = this.dataBaseManager.getAnswerEntitiesFromQuestionId(questionEntity.getQuestionId());

            for (AnswerEntity answerEntity : answerEntityList) {
                RetailerEntity retailerEntity = this.dataBaseManager.getRetailerEntityFromRetailerId(answerEntity.getRetailerId());
                RetailerModel retailerModel = new RetailerModel();
                retailerModel.setRetailerId(retailerEntity.getRetailerId());
                retailerModel.setRetailerName(retailerEntity.getShopName());
                retailerModel.setRetailerChatContent(answerEntity.getAnswerText());
                retailerModel.setChatTimeStamp(String.valueOf(answerEntity.getUpdatedAt()));
                retailerModel.setAnswerId(answerEntity.getAnswerId());
                retailerModelList.add(retailerModel);
            }

            productModel.setRetailerModels(retailerModelList);
            productModelList.add(productModel);
        }
        chatListener.onChatFetchSuccess(productModelList);
    }

    public void fetchChatOnline() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            chatListener.onChatFetchFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        chatListener.onChatFetchStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        getChatList(jsonResponse);
                        fetchChatOffline();
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatListener.onChatFetchFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatListener.onChatFetchFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String chatUrl = Constants.BASE_URL + Constants.CHAT_URL;
        LoginModel loginModel = ((LokalChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(chatUrl, null, getRequestBody(), HttpConnection.REQUEST_COMMON, loginModel);
    }

    private String getRequestBody() {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageCounter);
            jsonObject.put("size", "5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }

    public List<ProductModel> getChatList(String response) {
        List<ProductModel> productModelList = new ArrayList<ProductModel>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray contentArray = jsonObject.getJSONArray("content");
            for (int ii = 0; ii < contentArray.length(); ii++) {
                //ChatModel chatModel = new ChatModel();
                JSONObject contentObject = contentArray.getJSONObject(ii);
                JSONObject productObject = contentObject.getJSONObject("product");

                ProductModel productModel = new ProductModel();
                productModel.setConsumerChatId(contentObject.getString("id"));
                productModel.setProductId(productObject.getString("id"));
                productModel.setProductName(productObject.getString("productName"));
                //productModel.setChatTimeStamp(productObject.getString("lastModifiedDate"));

                ProductEntity productEntity = new ProductEntity(productModel.getProductId(), "", "", productModel.getProductName(), "", "");


                List<RetailerModel> retailerModelList = new ArrayList<RetailerModel>();
                List<AnswerEntity> answerEntityList = new ArrayList<AnswerEntity>();
                List<RetailerEntity> retailerEntityList = new ArrayList<RetailerEntity>();
                JSONArray answerArray = contentObject.getJSONArray("answers");
                for (int jj = 0; jj < answerArray.length(); jj++) {
                    JSONObject answerObject = answerArray.getJSONObject(jj);
                    JSONObject retailerObject = answerObject.getJSONObject("retailer");
                    RetailerModel retailerModel = new RetailerModel();
                    retailerModel.setRetailerId(retailerObject.getString("id"));
                    retailerModel.setRetailerName(retailerObject.getString("shopName"));
                    retailerModel.setRetailerChatContent(answerObject.getString("answerText"));
                    retailerModel.setChatTimeStamp(answerObject.getString("lastModifiedDate"));
                    retailerModel.setAnswerId(answerObject.getString("id"));
                    retailerModelList.add(retailerModel);

                    AnswerEntity answerEntity = new AnswerEntity(retailerModel.getAnswerId(), retailerModel.getRetailerId(),
                            productModel.getConsumerChatId(), retailerModel.getRetailerChatContent(), retailerModel.getChatTimeStamp());
                    productModel.setChatTimeStamp(retailerModel.getChatTimeStamp());
                    answerEntityList.add(answerEntity);
                    RetailerEntity retailerEntity = new RetailerEntity(retailerModel.getRetailerId(), retailerModel.getRetailerName());
                    retailerEntityList.add(retailerEntity);

                }

                productModel.setRetailerModels(retailerModelList);
                productModel.setConsumerChatContent(contentObject.getString("questionText"));
                productModelList.add(productModel);

                QuestionEntity questionEntity = new QuestionEntity(productModel.getConsumerChatId(), productModel.getProductId(),
                        productModel.getConsumerChatContent(), 0, productModel.getChatTimeStamp());

                // DB insertions
                this.dataBaseManager.addQuestionEntity(questionEntity);
                this.dataBaseManager.addAnswerEntities((ArrayList<AnswerEntity>) answerEntityList);
                this.dataBaseManager.addRetailerEntities((ArrayList<RetailerEntity>) retailerEntityList);
                this.dataBaseManager.addProductEntity(productEntity);

                // DB Update
                for (AnswerEntity answerEntity : answerEntityList) {

                    if (TextUtils.isEmpty(this.dataBaseManager.getAnswerTextByAnswerId(answerEntity.getAnswerId())) &&
                            !TextUtils.isEmpty(answerEntity.getAnswerText())) {
                        this.dataBaseManager.updateAnswerEntity(answerEntity);
                        questionEntity.setUpdatedAt(answerEntity.getUpdatedAt());
                        this.dataBaseManager.updateQuestionEntity(questionEntity);
                    }

                }

                JSONObject pageObject = jsonObject.getJSONObject("page");
                String totalPage = pageObject.getString("totalPages");
                Utils.getShopChatApplication((Activity) context).setNumberOfChatPage(Integer.parseInt(totalPage));

                String currentPage = pageObject.getString("number");
                Utils.getShopChatApplication((Activity) context).setCurrentChatPageNumber(Integer.parseInt(currentPage));

                String totalOnlineRecords = pageObject.getString("totalElements");
                Utils.getShopChatApplication((Activity) context).setTotalOnlineRecord(Integer.parseInt(totalOnlineRecords));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return productModelList;

    }

    private MultiMap<String, ProductModel> getChatMap(String response) {
        MultiMap<String, ProductModel> chatMap = new MultiValueMap<String, ProductModel>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray contentArray = jsonObject.getJSONArray("content");
            for (int ii = 0; ii < contentArray.length(); ii++) {
                //ChatModel chatModel = new ChatModel();
                JSONObject contentObject = contentArray.getJSONObject(ii);
                JSONObject productObject = contentObject.getJSONObject("product");

                ProductModel productModel = new ProductModel();
                productModel.setProductId(productObject.getString("id"));
                productModel.setProductName(productObject.getString("productName"));
                productModel.setChatTimeStamp(productObject.getString("lastModifiedDate"));

                List<RetailerModel> retailerModelList = new ArrayList<RetailerModel>();
                JSONArray answerArray = contentObject.getJSONArray("answers");
                for (int jj = 0; jj < answerArray.length(); jj++) {
                    JSONObject answerObject = answerArray.getJSONObject(jj);
                    JSONObject retailerObject = answerObject.getJSONObject("retailer");
                    RetailerModel retailerModel = new RetailerModel();
                    retailerModel.setRetailerId(retailerObject.getString("id"));
                    retailerModel.setRetailerName(retailerObject.getString("shopName"));
                    retailerModel.setRetailerChatContent(answerObject.getString("answerText"));
                    retailerModel.setChatTimeStamp(answerObject.getString("lastModifiedDate"));
                    retailerModelList.add(retailerModel);
                }

                productModel.setRetailerModels(retailerModelList);
                productModel.setConsumerChatContent(contentObject.getString("questionText"));
                chatMap.put(productModel.getProductId(), productModel);

                JSONObject pageObject = jsonObject.getJSONObject("page");
                String totalPage = pageObject.getString("totalPages");
                Utils.getShopChatApplication((Activity) context).setNumberOfChatPage(Integer.parseInt(totalPage));

                String currentPage = pageObject.getString("number");
                Utils.getShopChatApplication((Activity) context).setCurrentChatPageNumber(Integer.parseInt(currentPage));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return chatMap;
    }

}
