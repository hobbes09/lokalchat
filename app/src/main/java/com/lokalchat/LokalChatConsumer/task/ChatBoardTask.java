package com.lokalchat.LokalChatConsumer.task;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.lokalchat.LokalChatConsumer.LokalChatApplication;
import com.lokalchat.LokalChatConsumer.R;
import com.lokalchat.LokalChatConsumer.listener.ChatBoardListener;
import com.lokalchat.LokalChatConsumer.models.ChatBoardModel;
import com.lokalchat.LokalChatConsumer.models.ErrorModel;
import com.lokalchat.LokalChatConsumer.models.LoginModel;
import com.lokalchat.LokalChatConsumer.models.ProductModel;
import com.lokalchat.LokalChatConsumer.models.RetailerModel;
import com.lokalchat.LokalChatConsumer.network.HttpConnection;
import com.lokalchat.LokalChatConsumer.utils.Constants;
import com.lokalchat.LokalChatConsumer.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Sudipta on 9/27/2015.
 */
public class ChatBoardTask {

    private Context context;
    private HttpConnection httpConnect;
    private ChatBoardListener chatBoardListener;
    private String message;
    private RetailerModel retailerModel;
    private ProductModel productModel;
    private boolean isFromUnsentPool;

    public ChatBoardTask(Context context, ChatBoardListener chatBoardListener, boolean isFromUnsentPool) {
        this.context = context;
        this.chatBoardListener = chatBoardListener;
        this.isFromUnsentPool = isFromUnsentPool;
        this.message = message;
        this.retailerModel = retailerModel;
        this.productModel = productModel;

    }

    public void sendChat() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            chatBoardListener.onSendChatFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        chatBoardListener.onSendChatStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        if (!isFromUnsentPool) {
                            chatBoardListener.onSendChatSuccess(Utils.getShopChatApplication((Activity) context).getChatBoardModels().get(0));
                        } else {
                            chatBoardListener.onSendChatSuccess(Utils.getShopChatApplication((Activity) context).getUnsentChatList().get(0));
                        }

                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatBoardListener.onSendChatFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatBoardListener.onSendChatFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String chatSubmitUrl = Constants.BASE_URL + Constants.CHAT_SUBMIT_URL;
        LoginModel loginModel = ((LokalChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(chatSubmitUrl, null, getRequestBody(), HttpConnection.REQUEST_COMMON, loginModel);
    }

    private String getRequestBody() {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("question", getMessage());
            jsonObject.put("retailers", getRetailerId());
            jsonObject.put("product", getProduct());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }

    private String getRetailerId() {
        StringBuilder retailerBuilder = new StringBuilder();
        List<ChatBoardModel> chatBoardList = null;

        if (!isFromUnsentPool) {
            chatBoardList = Utils.getShopChatApplication((Activity) this.context).getChatBoardModels();
            for (ChatBoardModel chatBoardModel : chatBoardList) {
                if (!chatBoardModel.isCustomer()) {
                    if (chatBoardModel.getRetailerModel() != null) {
                        retailerBuilder.append(chatBoardModel.getRetailerModel().getRetailerId());
                        retailerBuilder.append(",");
                    }
                }
            }

        } else {
            chatBoardList = Utils.getShopChatApplication((Activity) this.context).getUnsentChatList();
            for (ChatBoardModel chatBoardModel : chatBoardList) {
                if (chatBoardModel.isCustomer()) {
                    List<RetailerModel> unsentRetailer = chatBoardModel.getRetailerModelList();
                    for (RetailerModel retailerModel : unsentRetailer) {
                        retailerBuilder.append(retailerModel.getRetailerId());
                        retailerBuilder.append(",");
                    }
                }
            }

        }


        return retailerBuilder.toString();
    }

    private String getProduct() {
        ChatBoardModel chatBoardModel = null;
        if (!isFromUnsentPool) {
            List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getChatBoardModels();
            chatBoardModel = chatBoardList.get(0);
        } else {
            List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getUnsentChatList();
            chatBoardModel = chatBoardList.get(0);
        }

        return chatBoardModel.getProductModel().getProductName();

    }

    private String getMessage() {
        ChatBoardModel chatBoardModel = null;
        if (!isFromUnsentPool) {
            List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getChatBoardModels();
            chatBoardModel = chatBoardList.get(0);
        } else {
            List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getUnsentChatList();
            chatBoardModel = chatBoardList.get(0);
        }

        return chatBoardModel.getChatContent();
    }

}
