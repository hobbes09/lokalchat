package com.lokalchat.LokalChatConsumer.models.entities;

/**
 * Created by sourin on 02/12/15.
 */
public class AnswerEntity {

    private String answerId;
    private String retailerId;
    private String questionId;
    private String answerText;
    private String updatedAt;

    public AnswerEntity(String answerId, String retailerId, String questionId, String answerText, String updatedAt) {
        this.answerId = answerId;
        this.retailerId = retailerId;
        this.questionId = questionId;
        this.answerText = answerText;
        this.updatedAt = updatedAt;
    }

    public AnswerEntity(){

    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
