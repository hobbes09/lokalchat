package com.lokalchat.LokalChatConsumer.models.entities;

/**
 * Created by sourin on 02/12/15.
 */
public class QuestionEntity {

    private String questionId;
    private String productId;
    private String questionText;
    private int seen;
    private String updatedAt;

    /* initializing 1 as per assumption that chat will be done while app is in online mode. Hence considering
     the value 1 is sending chat is successful.*/

    private int sent = 1;

    public QuestionEntity(String questionId, String productId, String questionText, int seen, String updatedAt) {
        this.questionId = questionId;
        this.productId = productId;
        this.questionText = questionText;
        this.seen = seen;
        this.updatedAt = updatedAt;
    }

    public QuestionEntity(String questionId, String productId, String questionText, int seen, String updatedAt, int sent) {
        this.questionId = questionId;
        this.productId = productId;
        this.questionText = questionText;
        this.seen = seen;
        this.updatedAt = updatedAt;
        this.sent = sent;
    }

    public QuestionEntity() {
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public int getSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }
}
