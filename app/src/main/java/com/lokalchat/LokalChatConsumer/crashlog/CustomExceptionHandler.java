package com.lokalchat.LokalChatConsumer.crashlog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.DateFormat;
import java.util.Date;

public class CustomExceptionHandler implements UncaughtExceptionHandler {

    private UncaughtExceptionHandler defaultUEH;

    /* 
     * if any of the parameters is null, the respective functionality 
     * will not be used 
     */
    public CustomExceptionHandler() {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread t, Throwable e) 
    {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();
        
        DateFormat[] formats = new DateFormat[] 
         {        		  
        	   DateFormat.getDateTimeInstance(),        		   
         };
        String time = "";
    	 for (DateFormat df : formats) 
    	 {
    		   time += df.format(new Date(System.currentTimeMillis()));
    	 }
        	 
        stacktrace += " \nCrash Happen: At :"+time;

    	try {
			writeFile(stacktrace);
		} catch (IOException e1) {				
			e1.printStackTrace();
		}       
        defaultUEH.uncaughtException(t, e);
    }

    private void writeFile(String data) throws IOException
    {
    	File myFile = new File("/sdcard/LokalChatConsumerLog.txt");
    	if(!myFile.exists())
        myFile.createNewFile();
        FileOutputStream fOut = new FileOutputStream(myFile);
        OutputStreamWriter myOutWriter = 
                                new OutputStreamWriter(fOut);
        myOutWriter.append(data);
        myOutWriter.close();
        fOut.close();
    }
}
//    private void writeToFile(String stacktrace, String filename) 
//    {
//        try {
//            BufferedWriter bos = new BufferedWriter(new FileWriter(
//                    localPath + "/" + filename));
//            bos.write(stacktrace);
//            bos.flush();
//            bos.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private void sendToServer(String stacktrace, String filename) {
//        DefaultHttpClient httpClient = new DefaultHttpClient();
//        HttpPost httpPost = new HttpPost(url);
//        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//        nvps.add(new BasicNameValuePair("filename", filename));
//        nvps.add(new BasicNameValuePair("stacktrace", stacktrace));
//        try {
//            httpPost.setEntity(
//                    new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
//            httpClient.execute(httpPost);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }}

